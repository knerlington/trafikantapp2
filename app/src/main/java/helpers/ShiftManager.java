package helpers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import models.Entry;
import models.Shift;

/**
 * Created by knerlington on 2015-11-25.
 */
public class ShiftManager {
    private static ShiftManager instance;
    private ArrayList<Shift> shiftList;

    private Shift currentShift;
    public Shift getCurrentShift() {
        return currentShift;
    }

    private File currentFile;

    public File getCurrentFile() {
        return currentFile;
    }

    public void setCurrentFile(File currentFile) {
        this.currentFile = currentFile;
    }

    private ShiftManager(){
        this.shiftList = new ArrayList<>();
    }
    public static ShiftManager getInstance() {
        if(instance == null){
            instance = new ShiftManager();
        }
        return instance;
    }

    public void addShift(Shift shift){
        this.currentShift = shift;
        this.shiftList.add(shift);

    }

     public void writeEntryToCurrentShift(String text){
         this.currentShift.getEntries().add(new Entry(this.currentShift.getDateString(), this.currentShift.getLocation(), this.currentShift.getName(), text, this.currentShift.getEntries().size()));
         this.currentShift.setCurrentEntry(this.currentShift.getEntries().get(this.currentShift.getEntries().size()-1));

         try{
             BufferedWriter writer = new BufferedWriter(new FileWriter(this.currentFile, true /*append*/));
             if(this.currentShift.getEntries().size() > 1){
                 writer.append("\n"); //append new line
             }

             writer.append(String.valueOf(this.currentShift.getCurrentEntry().getIndex()+1)); //write index
             writer.append(",");

             writer.append(this.currentShift.getDateString()); //write date
             writer.append(",");

             writer.append(this.currentShift.getLocation()); //write location
             writer.append(",");

             writer.append(this.currentShift.getName()); //write name
             writer.append(",");

             writer.append(text); //write text
             writer.append(",");

             writer.close();
         }catch(IOException e){

         }
    }
}
