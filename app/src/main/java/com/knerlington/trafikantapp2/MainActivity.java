package com.knerlington.trafikantapp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import helpers.FSHelper;
import helpers.ShiftManager;
import models.Shift;

public class MainActivity extends AppCompatActivity {
    private FSHelper fsHelper;
    private ShiftManager manager;
    private EditText editTextName, editTextLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fsHelper = new FSHelper();
        fsHelper.createDirInPublicPrimaryExtRootStorage("TrafikantAppen2");
        manager = ShiftManager.getInstance();
        editTextLocation = (EditText)findViewById(R.id.editText_location);
        editTextName = (EditText)findViewById(R.id.editText_name);

    }

    public void createShift(View v){
        switch (v.getId()){
            case R.id.button_create_shift:
                manager.addShift(new Shift(editTextName.getText().toString(), editTextLocation.getText().toString()));
                manager.setCurrentFile(fsHelper.createFileInCustomDir("TrafikantAppen2", manager.getCurrentShift().getFileName()));
                Intent intent = new Intent(this,EntryActivity.class);
                startActivity(intent);
                break;
        }
    }


}
