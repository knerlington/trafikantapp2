package com.knerlington.trafikantapp2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import helpers.ShiftManager;
import models.Shift;

public class EntryActivity extends AppCompatActivity  {
    private ShiftManager manager;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);
        manager = ShiftManager.getInstance();
        //Determine screen size
        if((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE){

            //find all buttons in rootView
            LinearLayout rootView = (LinearLayout)findViewById(R.id.root);
            ArrayList<Button> buttons = new ArrayList<>();
            for(int i = 0; i < rootView.getChildCount();i++){
                View v = rootView.getChildAt(i);
                if(v instanceof Button){
                    Button b = (Button)v;
                    buttons.add(b);
                }
            }
            //resize each drawable in each button
            for(Button b:buttons){
                Drawable drawable = b.getCompoundDrawables()[0];
                int imgHeight = drawable.getIntrinsicHeight();
                int imgWidth = drawable.getIntrinsicWidth();
                //for dpToPixel look here: http://stackoverflow.com/a/6327095/2306907
                float scale = (float) (convertDpToPixel(110, this)) / imgHeight;
                Rect rect = drawable.getBounds();
                int newWidth = (int)(imgWidth * scale);
                int newHeight = (int)(imgHeight * scale);
                rect.left = rect.left + (int)(0.5 * (imgWidth - newWidth));
                rect.top = rect.top + (int)(0.5 * (imgHeight - newHeight));
                rect.right = rect.left + newWidth;
                rect.bottom = rect.top + newHeight;
                b.setPadding((int) convertDpToPixel(40, this), 0, (int)convertDpToPixel(20,this), 0);
                drawable.setBounds(rect);
            }

        }
    }
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }
    @Override
    public void onBackPressed(){
        //Do nothing
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_entry_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement

        switch (item.getItemId()){
            case R.id.action_endShift:
                //end shift
                this.endPassAlert();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPause () {
        super.onPause();
       this.endPassAlert();
    }

    private void endPassAlert() {
        //create alert dialog
        if (dialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Vill du avsluta passet?").setTitle("Avsluta pass");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    Intent intent = new Intent(EntryActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
            builder.setNegativeButton("Avbryt", null);
            dialog = builder.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
        }
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
    /*
   * invoked in android:onClick attr for all buttons in activity_main*/
    public void createEntry(View v){
        String text;
        switch (v.getId()){
            case R.id.button1:
                //do stuff
                text = getString(R.string.button1);
                manager.writeEntryToCurrentShift(text);
                Toast.makeText(EntryActivity.this, "Inlägg A skapat!", Toast.LENGTH_SHORT).show();

                break;
            case R.id.button2:
                //do stuff
                text = getString(R.string.button2);
                manager.writeEntryToCurrentShift(text);
                Toast.makeText(EntryActivity.this, "Inlägg B skapat!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.button3:
                //do stuff
                text = getString(R.string.button3);
                manager.writeEntryToCurrentShift(text);
                Toast.makeText(EntryActivity.this, "Inlägg C skapat!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.button4:
                //do stuff
                text = getString(R.string.button4);
                manager.writeEntryToCurrentShift(text);
                Toast.makeText(EntryActivity.this, "Inlägg D skapat!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.button5:
                //do stuff
                text = getString(R.string.button5);
                manager.writeEntryToCurrentShift(text);
                Toast.makeText(EntryActivity.this, "Inlägg E skapat!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.button6:
                text = getString(R.string.button6);
                manager.writeEntryToCurrentShift(text);
                Toast.makeText(EntryActivity.this, "Inlägg F skapat!", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
