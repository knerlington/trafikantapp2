package models;

/**
 * Created by knerlington on 2015-11-25.
 */
public class Entry {
    private String date, location, name, text;
    private int index;

    public int getIndex() {
        return index;
    }

    public Entry(String date, String location, String name, String text, int index){
        this.date = date;
        this.location = location;
        this.name = name;
        this.text = text;
        this.index = index;
    }
}
