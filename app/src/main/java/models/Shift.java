package models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by knerlington on 2015-11-25.
 */
public class Shift {
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss");public SimpleDateFormat getDateFormat(){return dateFormat;}
    private Date date;public Date getDate() {return date;}
    private String dateString;public String getDateString(){return dateString;}
    private String fileName;
    public String getFileName() {
        return fileName;
    }

    private Entry currentEntry;

    public Entry getCurrentEntry() {
        return currentEntry;
    }

    public void setCurrentEntry(Entry currentEntry) {
        this.currentEntry = currentEntry;
    }

    private final String fileExtension = ".csv";

    private ArrayList<Entry> entries;

    public ArrayList<Entry> getEntries() {
        return entries;
    }

    private String location;
    public String getLocation() {
        return location;
    }

    private String text;
    public String getText() {
        return text;
    }

    private String name;
    public String getName() {
        return name;
    }

    public Shift(String name,String location){
        this.name = name;
        this.entries = new ArrayList<>();
        this.date = new Date();
        this.dateString = getDateFormat().format(getDate());
        this.location = location;
        fileName = String.format("%s%s%s%s", getLocation(),"_"+getName(),"_"+getDateString(),fileExtension);
    }

}
